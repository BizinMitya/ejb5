package query;

import model.Employee;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public class QueryBean implements SessionBean {
    public static final String ID_QUERY = "SELECT `empno`, `ename`, `job`,`hiredate`, `deptno`  FROM `netcracker`.`emp` WHERE empno=?";
    public static final String ALL_QUERY = "SELECT `empno`, `ename`, `job`,`hiredate`, `deptno` FROM `netcracker`.`emp`";
    public static final String NAME_QUERY = "SELECT `empno`, `ename`, `job`,`hiredate`, `deptno`  FROM `netcracker`.`emp` WHERE ename=?";

    public QueryBean() {

    }

    public Connection getConnection() throws SQLException, NamingException {
        return ((DataSource) new InitialContext().lookup("java:/MySqlDS")).getConnection("root", "jExonyz6");
    }

    @Override
    public void setSessionContext(SessionContext sessionContext) {

    }

    @Override
    public void ejbRemove() {

    }

    @Override
    public void ejbActivate() {

    }

    @Override
    public void ejbPassivate() {

    }

    public Employee queryById(int id) {
        Employee employee = null;
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(ID_QUERY);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                employee = new Employee(resultSet.getInt("empno"), resultSet.getString("ename"), resultSet.getString("job"),
                        resultSet.getString("hiredate"), resultSet.getInt("deptno"));
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }
        return employee;
    }

    public List<Employee> queryByName(String name) {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(NAME_QUERY);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(resultSet.getInt("empno"), resultSet.getString("ename"), resultSet.getString("job"),
                        resultSet.getString("hiredate"), resultSet.getInt("deptno")));
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }
        return employees;
    }

    public List<Employee> queryAll() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(ALL_QUERY);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(resultSet.getInt("empno"), resultSet.getString("ename"), resultSet.getString("job"),
                        resultSet.getString("hiredate"), resultSet.getInt("deptno")));
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }
        return employees;
    }

    public void ejbCreate() {

    }
}
