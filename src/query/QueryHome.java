package query;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public interface QueryHome extends EJBHome {
    Query create() throws RemoteException, CreateException;
}
