package query;

import model.Employee;

import javax.ejb.EJBObject;
import javax.naming.NamingException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public interface Query extends EJBObject {
    Employee queryById(int id) throws RemoteException;

    List<Employee> queryByName(String name) throws RemoteException;

    List<Employee> queryAll() throws RemoteException;

    Connection getConnection() throws SQLException, NamingException, RemoteException;
}
