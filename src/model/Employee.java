package model;

import java.io.Serializable;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public class Employee implements Serializable {
    private int empno;
    private String ename;
    private String job;
    private String hiredate;
    private int deptno;

    public Employee(int empno, String ename, String job, String hiredate, int deptno) {
        this.setEmpno(empno);
        this.setEname(ename);
        this.setJob(job);
        this.setHiredate(hiredate);
        this.setDeptno(deptno);
    }

    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }
}
