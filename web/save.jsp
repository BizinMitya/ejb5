<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.Locale" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 04.04.2016
  Time: 22:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="javascript/jquery-2.2.2.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <meta charset="utf-8">
    <title>Сохранить?</title>
</head>
<body>
<form name="save_form" action="index.jsp" method="post">
    <p>Имя сотрудника
        <input class="input_text" id="input_name" name="nameQuery" type="text"></p>
    <script>
        input_name.value = decodeURIComponent(location.search.substring(1).split('=')[1]);
    </script>
    <p><input class="button" type="submit" onclick="window.opener.close();" value="Сохранить"></p>
</form>
</body>
</html>
